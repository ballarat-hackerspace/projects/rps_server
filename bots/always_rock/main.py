import os

class RockBot:
    bot_number: int  # Passed in when the bot is created. Don't need to touch
    opponents_history: list

    def __init__(self, my_bot_number):
        self.bot_number = my_bot_number
        self.opponents_history = []

    def play_round(self) -> str:
        """Wins one game of rock paper scissors"""
        return 'r'

    def update_history(self, bot1, bot2):
        if self.bot_number == 1:
            self.opponents_history.append(bot1)
        else:
            self.opponents_history.append(bot2)


# Instantiate your bot using this code. Make sure its called `bot`
bot = RockBot(os.getenv("BOT_NUMBER"))

###########################################
##  Unless you know what you are doing,  ##
##  probably don't touch anything below  ##
##  this line.                           ##
###########################################

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port_number = int(os.getenv("PORT", 50000))
s.bind(('0.0.0.0', port_number))
s.listen(1)
conn, addr = s.accept()
while 1:
    data_bytes = conn.recv(16)

    data = data_bytes.decode()
    data = data.strip()
    print(f"received data: {data}")

    if data == "q":
        break

    if "," in data:
        # Get guesses from string, like p,r (for bot1 chose paper, bot2 chose rock)
        bot1, bot2 = data[0], data[2]

    if "?" in data:
        choice = bot.play_round()
        print("guessing", choice)
        conn.send(choice.encode())

    if not data:
        break
conn.close()
