import os
import random
from collections import Counter

choices = 'rps'

beaten_by = {
    # what: what_beats_it
    'r': 'p',
    'p': 's',
    's': 'r'
}


class BestMostCommonBot:
    bot_number: int  # Passed in when the bot is created. Don't need to touch
    opponents_history: list

    def __init__(self, my_bot_number):
        self.bot_number = my_bot_number
        self.opponents_history = []

    def play_round(self) -> str:
        """Plays one game of rock paper scissors"""
        # Compute how many times each option was chosen by the opponent. if a tie, pick one at random
        counts = Counter(self.opponents_history)
        if counts:
            max_value = max(counts.values())
            print(counts)
            most_commonly_chosen = [key for key, value in counts.items() if value == max_value]
        else:
            most_commonly_chosen = 'rps'
        most_commonly_chosen_option = random.choice(most_commonly_chosen)

        # Return the option that beats that option
        my_choice = beaten_by[most_commonly_chosen_option]
        return my_choice

    def update_history(self, bot1, bot2):
        if self.bot_number == 1:
            self.opponents_history.append(bot2)
        else:
            self.opponents_history.append(bot1)


# Instantiate your bot using this code. Make sure its called `bot`
bot = BestMostCommonBot(os.getenv("BOT_NUMBER"))

###########################################
##  Unless you know what you are doing,  ##
##  probably don't touch anything below  ##
##  this line.                           ##
###########################################

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port_number = int(os.getenv("PORT", 50000))
s.bind(('0.0.0.0', port_number))
s.listen(1)
conn, addr = s.accept()
while 1:
    data_bytes = conn.recv(16)

    data = data_bytes.decode()
    data = data.strip()
    print(f"received data: {data}")

    if data == "q":
        break

    if "," in data:
        # Get guesses from string, like p,r (for bot1 chose paper, bot2 chose rock)
        bot1, bot2 = data[0], data[2]
        bot.update_history(bot1, bot2)

    if "?" in data:
        choice = bot.play_round()
        print("guessing", choice)
        conn.send(choice.encode())

    if not data:
        break
conn.close()
