# Setup a development environment using docker images

echo "Building bots"

echo "I am the rock"
cd bots/always_rock
docker build --tag ${DOCKER_NAME}/always_rock:latest .
docker push ${DOCKER_NAME}/always_rock:latest

echo "I am the brains"
cd ../bots/best_most_common
docker build --tag ${DOCKER_NAME}/best_most_common:latest .
docker push ${DOCKER_NAME}/best_most_common:latest


echo "I am random lol"
cd ../bots/random_server
docker build --tag ${DOCKER_NAME}/random_server:latest .
docker push ${DOCKER_NAME}/random_server:latest
