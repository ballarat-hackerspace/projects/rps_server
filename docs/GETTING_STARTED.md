

# Make your own bot!

1. Have a look at the bots in the bots folder (best_most_common would be a good candidate)
2. Copy one that is like yours and modify to suit.
3. Run this to build, change rps:random to your own tag: `docker build . --tag myusername/rps:latest`

A really basic bot, but still reads the input file, is `best_most_common`, which reads the game history so far, finds 
the opponent's most frequent choice, and beats that.

Your bot simply returns "r", "p", or "s".

The runner script will strip any whitespace (usually a newline) from the end of your bot's output, and take the last
character (*only*) as the option selected by your bot.


## What will be run
At this stage, the command being sent to the docker file will always be: `python /usr/app/main.py`
You can call whatever you want in python, but it starts with that command.

Here is the command that your docker file will actually be run with (see security below):

```bash
docker run --memory=1g --cpus=1 --stop-timeout=1 --net=host -e PORT=12345 -e BOT_NUMBER=1 -it TAG python /usr/app/main.py
```

Bot number will be 1 or 2 (see below). The Port number will be given to the bot, so make sure the bot can run on any port
(the best_most_common bot handles this, just copy that code).


## Important things to check:

- Your docker should print (to stdout) just one character. Only the first character is read as your option
- Valid options are (all lowercase): r (rock), p (paper), s (scissors)
- Output an invalid option is an auto-loss (for both sides if both are invalid!)
- Your docker image will get an automatic tag, so don't rely on the tag name or anything

Note that you can't beat random any more than 33% of the time on average.


## Security things:
- Your docker image won't have network/internet access
- Your docker image will not have access to files, except the history file passed in
- Limit of 1GB Ram
- Limit of 1 CPU
- Limit of 5 seconds to start your bot (TODO)
- Limit of 1 second per round (TODO)


# Upload your bot

At this stage, we only accept a docker image tag. This means you need to build your bot.
If you know docker already, the quick version is: build, upload to docker.io, note the tag.

If you need info, here it is:

When in your bot's directory (the one with a dockerfile), run:

```bash
docker build --tag YOUR_DOCKER_NAME/YOUR_BOTS_REPO:YOUR_BOTS_TAG .
```

You can create a repo at hub.docker.io.

If you don't know what tag to use, and aren't trying to do anything fancy, use `latest`, as that's the default one that docker will pull from later.

(Don't forget the . at the end.)

Your username, repo name, and tag must all **match** this regex:

```regexp
^[A-Za-z_][A-Za-z0-9_]*$
```

When its built, upload it to docker:


```bash
docker push YOUR_DOCKER_NAME/YOUR_BOTS_REPO:YOUR_BOTS_TAG 
```

Before you do that though, you need to have logged in. You'll need an account at hub.docker.com to create one.

```bash
docker login
# Then follow instructions
```

At this stage, only hub.docker.com accounts are supported - more can come later.


# Run your own battles!

To run your own battles, update the code in `battlemaster/run_battle.py` - specifically, change the bot tags at the bottom
of the file. Run this file. Make sure the docker tags are accessible on your system, either by uploading to docker, or 
by running them locally.
