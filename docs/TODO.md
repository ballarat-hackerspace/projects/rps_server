
# TODO for version 1
[ ] Deploy script to get running on oasis: https://www.docker.com/blog/how-to-deploy-on-remote-docker-hosts-with-docker-compose/
[X] Get base docker-compose working (persistent data, redis queue)
[ ] Get docker-compose setup able to launch sibling docker containers (i.e. the bots)
[ ] Automatically run battles in the background - i.e. between pairs of bots not tested
[X] User accounts / registering users / login
[ ] Reporting on results, battle state (in progress, complete)
[ ] Listing bots pagination (both server and client side)
[ ] Listing battles pagination (both server and client side)
[ ] Verify instructions are correct for GETTING_STARTED.md
[ ] When creating a new bot, run a `git pull` to get the newest version
[ ] Allow users to run `git pull` on their bots
[ ] Clean up old docker images if not needed

# Nice to haves
[ ] Test security really well, specifically what the docker bots can break
[ ] Allow docker repos on other urls
[ ] Support arbitrary commands for docker files, not just python main.py