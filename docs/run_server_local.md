
# Running a local dev server

Start by creating a .env file in the development folder:

```bash
cp development/template.env development/.env
```

Then modify the values as appropriate. Passwords in this file should be secret!

Once that one-time configuration is done, you can build the images.

```bash
docker-compose --env-file development/.env build
```

Do this when you make changes, such as to the webserver etc.

To run everything, from the root directory:

```bash
docker-compose --env-file development/.env up
```

(if you want in daemon mode, add `-d` to the end of that.)

Then, head to `http://127.0.0.1:5003/rps/` in your browser.

If you want to play around with the database, it is open on port 5432. 
The username and password are in the docker-compose file.


To start all services **except the web server**, run this:

```bash
docker-compose --env-file development/.env --profile no_web up
```


For developing the website, you might want to only run some services and not others.
You can do this with:

```bash
docker-compose --env-file development/.env up worker
```

Where worker is the name of the service you want to run.


## Getting some things setup

You might want a "quick" setup where you get some data etc going.

If that's of interest to you, the `setup_development.sh` script is for you!

Get setup with docker.io (the website, with a username/password), and then run:

```bash
DOCKER_NAME=YOUR_DOCKER_USERNAME ./development/setup_development.sh 
```

That script will build the three basic bots for you, and push them to your repo.