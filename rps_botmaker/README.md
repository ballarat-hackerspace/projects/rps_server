# RPS Botmaker


version number: v202112.0002
author: Rob Layton

## Overview

Common Functionality for Rock-Paper-Scissors bots

## Installation / Usage

To install use pip:

    $ pip install rps_botmaker

## Updating the version

`pycalver bump`


## Example

TBD
