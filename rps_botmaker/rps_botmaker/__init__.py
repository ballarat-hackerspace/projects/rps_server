__version__ = "v202112.0002"


VALID_CHOICES = {'r', 'p', 's'}


def validate_game_choice(choice:str) -> bool:
    """Ensures the choice is valid, that is, it is lowercase r, p or s.

    Parameters
    ----------
    choice: str

    Returns
    -------
    True if the choice is valid. Raises ValueError otherwise
    """
    if choice not in VALID_CHOICES:
        raise ValueError(f"Game Choice history was not r, p or s. Got {choice}")
    return True