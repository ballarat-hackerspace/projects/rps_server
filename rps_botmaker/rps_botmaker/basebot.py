import random
import socket
from typing import Tuple

from loguru import logger

from rps_botmaker import validate_game_choice

READ_SIZE_BYTES = 16
RPS_OPTIONS = ["r", "p", "s"]


class BaseBot:
    """Base class for Bots playing Rock-Paper-Scissors on the Ballarat Hackerspace Oasis server

    Parameters:
    -----------
    name:str
        Human readable name for the bot. Never rely on this for anything important!
    bot_number: int
        Either 1 or 2 only. If 1, the first input in `update_history` will be this bot's guess in the previous round.
        If 2, the second input will be this bot's guess.
    port_number: int
        Which port to listen to
    subnet_mask: str (default: "0.0.0.0")
        Which hosts to listen to. To listen to any incoming, keep as the default.

    """

    def __init__(self, name: str, bot_number: int, port_number: int, subnet_mask: str = "0.0.0.0"):
        self.name = name
        self.bot_number = bot_number
        self.port_number = port_number
        self.subnet_mask = subnet_mask

    def run(self):
        """Run the bot with a socket connection
        """
        conn = self._connect_socket()
        logger.info(f"Socket connected, starting to read")
        while 1:
            continue_game, data = self._handle_data(conn)
            if not data or not continue_game:
                break
        conn.close()

    def _handle_data(self, conn):
        data_bytes = conn.recv(READ_SIZE_BYTES)
        data = data_bytes.decode()
        data = data.strip()
        logger.debug(f"Received: {data}")
        response, continue_game = self._handle_single_command(data)
        if response:
            conn.send(response)
        return continue_game, data

    def _connect_socket(self):
        logger.info(f"{self.name} Starting")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.subnet_mask, self.port_number))
        s.listen(1)
        conn, _ = s.accept()
        return conn

    def _handle_single_command(self, command: str) -> Tuple[bytes, bool]:
        continue_game = False
        response = None
        if command == "q":
            logger.debug("Quitting due to input == 'q'")
            continue_game = False
        if "," in command:
            # Get guesses from string, like p,r (for bot1 chose paper, bot2 chose rock)
            bot1, bot2 = command[0], command[2]
            validate_game_choice(bot1)
            validate_game_choice(bot2)
            self.update_history(bot1, bot2)
            continue_game = True
        if "?" in command:
            choice = self.play_round()
            logger.debug(f"Guessing: {choice}")
            response = choice.encode()
            continue_game = True
        return response, continue_game

    def update_history(self, bot1_guess: str, bot2_guess: str) -> None:
        """Function to update the bot's history of the match

        Override this function to allow your bot to remember past rounds and track its own progress.

        Parameters:
        -----------
        bot1_guess: str
            The guess for "bot 1" in the battle. See `self.bot_number` to determine which bot `self` is
        bot2_guess: str
            The guess for "bot 2" in the battle. See `self.bot_number` to determine which bot `self` is
        """
        # Current bot does not record history. See examples/best_most_common for an example of a bot that maintains
        # history.
        pass

    def play_round(self) -> str:
        """Play one round of RPS.

        This is the main function to override in your bot - it determines how your bot actually plays.

        The default implementation is a random choice.

        Returns
        -------
        str: The choice of the bot in the current round. Will always be one of "r", "p" or "s" (in lowercase!)
        """
        return random.choice(RPS_OPTIONS)
