import pytest

from rps_botmaker import validate_game_choice, VALID_CHOICES
from rps_botmaker.basebot import BaseBot


def test_basebot_functionality():
    """Tests the BaseBot has the necessary functionality. Doesn't run bot"""
    bot = BaseBot('base_tester', 1, 50_000)
    assert bot.name == 'base_tester'
    assert bot.bot_number == 1
    assert bot.port_number == 50_000
    assert bot.subnet_mask == '0.0.0.0'
    for p1 in 'rps':
        for p2 in 'rps':
            assert bot.update_history(p1, p2) is None  # this function doesn't remember
    for i in range(10):
        # Yes, this did find a bug!
        assert validate_game_choice(bot.play_round())


def test_basebot_commands():
    """Tests the BaseBot has the necessary functionality. Doesn't run bot"""
    bot = BaseBot('base_tester', 1, 50_000)

    # Quitting should stop the game, hence second response is False
    assert bot._handle_single_command('q') == (None, False)

    # A command means we have two inputs, store them, do not respond, but continue
    assert bot._handle_single_command('r,p') == (None, True)
    assert bot._handle_single_command('p,p') == (None, True)
    with pytest.raises(IndexError):
        # No second choice
        assert bot._handle_single_command('r,') == (None, True)
    with pytest.raises(ValueError):
        # No first choice, extra character to confuse
        assert bot._handle_single_command(',pp') == (None, True)

    seen_values = set()
    for i in range(100):
        choice, continue_game = bot._handle_single_command('?')
        choice = choice.decode()
        seen_values.add(choice)
        assert continue_game
        assert validate_game_choice(choice)

    # Check all 3 options were returns at least once
    # This can fail randomly, but the chance is very small (0.67**100 ~= 4.0502354e-18)
    assert seen_values == VALID_CHOICES
