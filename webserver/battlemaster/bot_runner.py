import socket
import subprocess
import re
import time

from contextlib import closing

command = (
    "cat {history_filename} |"  # Pipe the history file
    "timeout 5 docker run "  # Run the docker file, but only for a max of 5 seconds
    "--memory=1g --cpus=1 --stop-timeout=1 "  # 1gb memory, 1cpu, 1second wait after stop
    "-e BOT_NUMBER={bot_number} "  # Environment variable of which bot number this is
    "-i {bot_tag}  "  # Interactive mode (so we get the output), running the given bot
    "python /usr/app/main.py"  # which command to run on the bot
)

# Valid options
valid_bot_numbers = {1, 2}

bot_validation_regex = re.compile(r"^[A-Za-z_][A-Za-z0-9_]*/[A-Za-z_][A-Za-z0-9_]*:[A-Za-z_][A-Za-z0-9_]*$")


# TODO: List of valid docker usernames as a config file?


def run_bot(bot_tag, bot_number, history_filename):
    if bot_number not in valid_bot_numbers:
        raise ValueError(f"Invalid bot number {bot_number}")

    if not bot_validation_regex.match(bot_tag):
        raise ValueError(f"Bot tag failed to validate: {bot_tag}")

    full_command = command.format(
        bot_number=bot_number, bot_tag=bot_tag, history_filename=history_filename)

    result = subprocess.check_output(full_command, shell=True, text=True)
    print(f"{bot_tag} returned: {result}")
    result = result.strip()
    if result:
        return result[-1].lower()  # First character only, lowercased
    else:
        return None


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


def bot_run_test():
    # Run bot
    bot_port = find_free_port()
    bot_tag = "robertlayton1/best_most_common"
    command = (
        f"docker run -d --memory=1g --cpus=1 --stop-timeout=1 "
        f"--net=rps_network -e PORT={bot_port} -e BOT_NUMBER=1 -it "
        f"{bot_tag} python /usr/app/main.py"
    )
    print(command)

    p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
    print("Started bot")
    time.sleep(1)  # TODO: get a signal from the bot for "ready"

    # Run client to make predictions
    HOST = "127.0.0.1"
    bot_port = bot_port
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, bot_port))
        for i in range(10):
            s.sendall(b'?')
            data = s.recv(10)
            print(f"Received: {data}")
        s.sendall(b'q')


# bot_run_test()
