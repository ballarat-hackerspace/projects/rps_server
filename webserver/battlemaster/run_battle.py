import json
import os
import socket
import subprocess
import time
from collections import Counter
from typing import Tuple

from flask import current_app as app
from battlemaster.bot_runner import run_bot, find_free_port

valid_options = {'r', 'p', 's'}


def determine_winner(c1, c2):
    """returns 1 if c1 wins, 2 if c2 wins. Returns 0 if draw"""

    c1_valid = c1 in valid_options
    c2_valid = c2 in valid_options

    if not c1_valid and not c2_valid:
        return 0
    if not c1_valid:
        return 2
    if not c2_valid:
        return 1

    if c1 == 'r':  # Player 1 Rock
        # print("win")
        if c2 == 's':
            return 1
        elif c2 == 'p':
            return 2
    elif c1 == 's':  # Player 1 Scissors
        if c2 == 'p':
            return 1
        elif c2 == 'r':
            return 2
    elif c1 == 'p':  # Player 1 Paper
        if c2 == 'r':
            return 1
        elif c2 == 's':
            return 2
    # Draw!
    return 0


def run_single_round(bot1_socket, bot2_socket):
    # Ask both bots to give next prediction
    bot1_socket.sendall(b'?')
    bot2_socket.sendall(b'?')
    time.sleep(0.1)
    bot1_action = bot1_socket.recv(1024).decode()
    print("bot1:", bot1_action)
    bot2_action = bot2_socket.recv(1024).decode()
    print("bot2:", bot2_action)

    winner = determine_winner(bot1_action, bot2_action)
    print("winner:", winner)

    # Send round information to both bots
    round_info = f"{bot1_action},{bot2_action}".encode()

    bot1_socket.sendall(round_info)
    bot2_socket.sendall(round_info)

    return winner, round_info


def get_container_name(container_id):
    command = f"docker inspect {container_id}"
    p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
    stdout, stderr = p.communicate()
    d = json.loads(stdout.decode())
    # app.logger.warning(d[0]['NetworkSettings'])
    return d[0]['NetworkSettings']['Networks']["rps_network"]['IPAddress']
    # return d[0]['Name'].replace("/", '')


def start_bot(bot_tag, bot_number) -> Tuple[str, int]:
    bot_port = find_free_port()
    command = (
        f"docker run -d "
        # f" --memory=1g "
        f" --cpus=1 --stop-timeout=1 --ulimit cpu=3600 "
        f" --network=rps_network -e PORT={bot_port} -e BOT_NUMBER={bot_number} -it "
        f" {bot_tag} python /usr/app/main.py"
    )
    app.logger.warning("Running bot:")
    app.logger.warning(f"{bot_tag} :{bot_port}")
    app.logger.warning(command)
    p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, )

    # TODO: Remove after debugging
    stdout, stderr = p.communicate()
    app.logger.warning("After attempting to run:")
    app.logger.warning("STDOUT:")
    app.logger.warning(stdout)

    app.logger.warning("STDERR")
    app.logger.warning(stderr)

    # Convert the docker is (in stdout) to the docker container name
    container_id = stdout.decode().strip()
    container_name = get_container_name(container_id)

    # TODO: end remove
    bot_address = container_name
    return bot_address, bot_port


def run_many_rounds(n_rounds, bot1_tag, bot2_tag):
    bot1_address, bot1_port = start_bot(bot1_tag, 1)
    bot2_address, bot2_port = start_bot(bot2_tag, 2)
    time.sleep(2)  # Give bots startup time

    winners = []
    rounds = []

    app.logger.warning(f"Connecting to bot1 on {bot1_address}:{bot1_port}")
    app.logger.warning(f"Connecting to bot2 on {bot2_address}:{bot2_port}")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as bot1_socket:
        bot1_socket.connect((bot1_address, bot1_port))
        app.logger.warning("Connected to bot1")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as bot2_socket:
            bot2_socket.connect((bot2_address, bot2_port))
            app.logger.warning("Connected to bot2")
            for round_number in range(n_rounds):
                app.logger.warning(f"Round {round_number}")
                winner, round_info = run_single_round(bot1_socket, bot2_socket)
                winners.append(winner)
                rounds.append(round_info)
    app.logger.warning(Counter(winners))
    return rounds


if __name__ == "__main__":
    n_rounds = 10
    bot1_tag = "robertlayton1/rps_random:latest"
    bot2_tag = "robertlayton1/best_most_common:latest"
    bot3_tag = "robertlayton1/always_rock:latest"
    bot4_tag = "robertlayton1/rps_random2:latest"

    run_many_rounds(n_rounds, bot1_tag, bot1_tag)
