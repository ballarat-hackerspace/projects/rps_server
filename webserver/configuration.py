import os

postgres_user = os.environ.get("POSTGRES_USER", "SET_PASSWORD_ENV_VAR")
postgres_password = os.environ.get("POSTGRES_PASSWORD", "rps_user")
postgres_db = os.environ.get("POSTGRES_DB", "rps")
postgres_host = os.environ.get("POSTGRES_HOST", "rps_db")


class ProductionConfig:
    SQLALCHEMY_DATABASE_URI = f'postgresql+pg8000://{postgres_user}:{postgres_password}@{postgres_host}/{postgres_db}'
    RQ_REDIS_URL = 'redis://rps_redis:6379/0'
    SECRET_KEY = os.environ.get("SECRET_KEY", "aosidfuap9sd8vp98sd9'asldf'as")
    URL_PREFIX = "/rps"


class LocalConfig:
    """For when you are running in the comfort of your own home,
    
    and have setup redis locally
    """
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    RQ_REDIS_URL = 'redis://localhost:6379/0'
    SECRET_KEY = 'a;lsdkfj;alskdjgbs8dfu9p8dfp;98duf'
    URL_PREFIX = "/rps"
