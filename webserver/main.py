import json
import os

from flask import Flask, request, render_template, redirect, flash, url_for
from flask_login import LoginManager, login_user, login_required, logout_user, current_user

import configuration
from battlemaster.run_battle import run_many_rounds, determine_winner
from models import db, Bot, BattleResult, User
import flask_rq2

app = Flask(__name__)
login_manager = LoginManager()

if os.environ.get("RPS_PRODUCTION"):
    app.config.from_object(configuration.ProductionConfig())
else:
    app.config.from_object(configuration.LocalConfig())


class PrefixMiddleware(object):

    def __init__(self, app, prefix=''):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):

        if environ['PATH_INFO'].startswith(self.prefix):
            environ['PATH_INFO'] = environ['PATH_INFO'][len(self.prefix):]
            environ['SCRIPT_NAME'] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response('404', [('Content-Type', 'text/plain')])
            return ["This url does not belong to the app.".encode()]


app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=app.config.get("URL_PREFIX", '/rps'))

# Register Components
login_manager.init_app(app)
db.init_app(app)
rq = flask_rq2.RQ(app)

login_manager.login_view = ".login"


@login_manager.user_loader
def load_user(user_id):
    user_id = int(user_id)
    return User.get(user_id)


@app.before_first_request
def setup_database():
    db.create_all()

    rob_user = User.get_by_nickname('robrps')
    if not rob_user:
        if "robrps_password" in os.environ:
            User.register('robrps', os.environ.get("robrps_password"))
            db.session.commit()


@app.after_request
def commit_to_db(response):
    # Commit any changes to database
    db.session.commit()
    return response


@app.route("/")
@login_required
def index():
    return render_template("index.html", bots=Bot.all())


@app.route("/bot/create", methods=['POST'])
@login_required
def create_bot():
    data = request.form

    bot = Bot(tag=data['tag'], nickname=data['nickname'], author=data['author'])
    db.session.add(bot)
    db.session.commit()
    flash(f"Bot {bot.bot_id} created! Good luck!", "success")

    return redirect(url_for("index"))


@app.route("/results/create", methods=['POST'])
@login_required
def create_battle():
    # TODO: Auth!
    data = request.form
    results = BattleResult()
    bot1 = Bot.get(int(data['bot1_id']))
    bot2 = Bot.get(int(data['bot2_id']))

    results.bots.append(bot1)
    results.bots.append(bot2)

    results.bot1_wins = 0
    results.bot2_wins = 0
    results.state = "Queued"
    db.session.add(results)
    db.session.commit()

    n_rounds = int(data.get('n_rounds', 5))

    # Queue the battle job
    job = run_battle.queue(results.battle_id, n_rounds)

    flash(f"Battle {results.battle_id} created! Good luck!", "success")

    return redirect(url_for("index"))


@rq.job
def run_battle(results_id, n_rounds):
    results = BattleResult.get(results_id)
    results.state = "Started"
    db.session.add(results)
    db.session.commit()

    if len(results.bots) == 2:
        # Expected case
        bot1, bot2 = results.bots
    elif len(results.bots) == 1:
        # Bot battling itself. This is a hack, because the association won't remember both
        bot1 = results.bots[0]
        bot2 = bot1
    else:
        raise ValueError(f"Number of bots should be two (or one, but not {len(results.bots)})")
    print(f"Running battle between {bot1.tag} and {bot2.tag}")
    round_info = run_many_rounds(n_rounds, bot1.tag, bot2.tag)
    round_info = [round.decode() for round in round_info]
    results.history = json.dumps(round_info)
    bot1_wins = 0
    bot2_wins = 0
    draws = 0
    for round_number in round_info:
        bot1_action, bot2_action = round_number.split(",")
        winner = determine_winner(bot1_action, bot2_action)
        if winner == 0:
            draws += 1
        elif winner == 1:
            bot1_wins += 1
        elif winner == 2:
            bot2_wins += 1
    results.bot1_wins = bot1_wins
    results.bot2_wins = bot2_wins
    results.draws = draws
    results.state = "Finished"
    db.session.add(results)
    try:
        db.session.commit()
        print(f"Result ID is {results.battle_id}")
        print(f"Finished battle between {bot1.tag} and {bot2.tag}")
        return results.battle_id
    except Exception as e:
        errors = [f"Unable to add item to database. {type(e)}: {e}"]
        return {"error": errors}


@app.route("/results")
@login_required
def get_all_results():
    battles = BattleResult.get_all_battles(most_recent_first=True, limit=10)
    print(battles)
    return render_template("results_list.html", results=battles)


@app.route("/results/get/<battle_id>")
@login_required
def get_results(battle_id):
    battle = BattleResult.get(battle_id)
    return battle.get_json()


@app.route("/auth/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        data = request.form
        remember_login = data['remember'] == 'on'
        # Attempt to login user
        user = User.login_check(data['nickname'], data['password'])
        if user:
            login_user(user, remember=remember_login)
            flash(f"Welcome {user.nickname}!", 'success')
            return redirect(url_for("index"))
        flash("Invalid login details", 'error')
    return render_template("login_page.html")


@app.route('/auth/register', methods=['POST'])
@login_required
def register():
    # TODO Currently, any logged in user can register another. This is by design, but might change when we go public?
    data = request.form
    # Check user doesn't already exist
    user = User.get_by_nickname(data['nickname'])
    if user:
        flash("Nickname already used", 'error')
    else:
        user = User.register(data['nickname'], data['password'])
        flash(f"User {user.nickname} created!")
    return redirect(url_for("index"))


@app.route('/auth/change_password', methods=['POST'])
@login_required
def change_password():
    data = request.form
    if current_user.check_password(data['current_password']):
        # User has correct current password
        if len(data['new_password']) >= 6:
            if data['new_password'] == data['check_new_password']:
                # Passwords are the same and passed basic validation
                current_user.set_password(data['new_password'])
                flash("Password changed", 'success')
            else:
                flash("New passwords didn't match", 'error')
        else:
            flash("Password needs to be at least 6 characters!")
    else:
        flash("Current password check failed!", 'error')
    return redirect(url_for("index"))


@app.route("/auth/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))
