from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash

db = SQLAlchemy()

battle_association_table = db.Table(
    'battle_association', db.Model.metadata,
    db.Column("mapping_id", db.Integer, primary_key=True),
    db.Column('bot_id', db.Integer, db.ForeignKey('bots.bot_id')),
    db.Column('battle_id', db.Integer, db.ForeignKey('battle_results.battle_id'))
)


class InvalidLoginException(Exception):
    pass


class Bot(db.Model):
    __tablename__ = "bots"

    bot_id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String)
    nickname = db.Column(db.String)
    author = db.Column(db.String)

    battles = relationship("BattleResult",
                           secondary=battle_association_table,
                           back_populates="bots")

    @staticmethod
    def get(id):
        return db.session.query(Bot).filter_by(bot_id=id).one()

    def get_json(self):
        return {
            'id': self.bot_id,
            'tag': self.tag,
            'nickname': self.nickname,
            'author': self.author
        }

    @staticmethod
    def all():
        return db.session.query(Bot).all()


class BattleResult(db.Model):
    __tablename__ = "battle_results"

    battle_id = db.Column(db.Integer, primary_key=True)

    bots = relationship("Bot",
                        secondary=battle_association_table,
                        back_populates="battles")

    state = db.Column(db.String)
    battle_date = db.Column(db.DateTime, default=db.func.now())
    bot1_wins = db.Column(db.Integer)
    bot2_wins = db.Column(db.Integer)
    draws = db.Column(db.Integer)
    history = db.Column(db.String)

    @staticmethod
    def get(battle_id):
        return db.session.query(BattleResult).filter(BattleResult.battle_id == battle_id).one()

    @staticmethod
    def get_all_battles(most_recent_first=True, limit=None):
        query = db.session.query(BattleResult)
        if most_recent_first:
            query = query.order_by(BattleResult.battle_date.desc())
        if limit:
            query = query.limit(limit)
        return query.all()

    def get_json(self):
        return {
            "battle_id": self.battle_id,
            "bots": [bot.get_json() for bot in self.bots],
            "bot1_wins": self.bot1_wins,
            "bot2_wins": self.bot2_wins,
            "draws": self.draws,
            "datetime": self.battle_date.isoformat(),
            "timestamp": self.battle_date.timestamp()
        }


class User(db.Model, UserMixin):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String)
    avatar = db.Column(db.String, nullable=True)
    hashed_password = db.Column(db.String)

    def get_id(self):
        return self.user_id

    def set_password(self, raw_password):
        self.hashed_password = generate_password_hash(raw_password, method='sha256')
        db.session.add(self)
        return self

    @staticmethod
    def register(nickname, raw_password, avatar=None):
        user = User.get_by_nickname(nickname)
        if user:
            # Already registered nickname
            raise InvalidLoginException("Nickname already registered")

        user = User()
        user.nickname = nickname
        user.avatar = avatar
        user.set_password(raw_password)
        db.session.add(user)
        db.session.commit()
        return user

    @staticmethod
    def get_by_nickname(nickname):
        try:
            return db.session.query(User).filter(User.nickname == nickname).one()
        except NoResultFound:
            return None

    @staticmethod
    def get(user_id):
        try:
            return db.session.query(User).filter(User.user_id == user_id).one()
        except NoResultFound:
            return None

    @staticmethod
    def login_check(nickname, raw_password):
        user = User.get_by_nickname(nickname)
        if user and user.check_password(raw_password):
            return user
        return None

    def check_password(self, raw_password):
        return check_password_hash(self.hashed_password, raw_password)
