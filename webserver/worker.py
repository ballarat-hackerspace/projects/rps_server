"""Creates a worker that handle jobs in ``default`` queue.

It's better to do this: flask rq worker
however, this script might be useful
"""

from main import rq, app

with app.app_context():
    default_worker = rq.get_worker('default')
    while True:
        default_worker.work(burst=False)
